import sys
sys.path.append('../')

import unittest
import DummyTest as dt

class Test_DummyTest(unittest.TestCase):
    def test_distinct(self):
        dto = dt.DummyTest()
        estimador = 10
        self.assertTrue(dto.distinct_x1(estimador))

    def test_igualdad(self):
        dto = dt.DummyTest()
        estimador = 2
        self.assertTrue(dto.equal_x2(estimador))
    
