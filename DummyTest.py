class DummyTest():
    def __init__(self):
        self.x1 = 1
        self.x2 = 2

    def distinct_x1(self, x):
        return x != self.x1

    def equal_x2(self, x):
        return x == self.x2